import { RedisGraph, Statistics, Record } from 'redisgraph.js';

RedisGraph.prototype.query = (async function(query) {
    const response = await this._sendCommand("graph.QUERY", [this._graphId, query]);
    return new ResultSet(response);
});

export class ResultSet {
    constructor(resp) {
        this._position = 0;
        this._statistics = new Statistics(resp[2]);

        const result = resp[1];

        // Empty result set
        if (!result || result.length === 0) {
            this._header = [];
            this._totalResults = 0;
            this._results = [];
        } else {
            this._header = resp[0];
            this._totalResults = result.length;
            this._results = new Array(this._totalResults);
            for (let i = 0; i < this._totalResults; ++i) {
                this._results[i] = new Record(this._header, result[i]);
            }
        }
    }

    getHeader() {
        return this._header;
    }

    hasNext() {
        return this._position < this._totalResults;
    }

    next() {
        return this._results[this._position++];
    }

    getStatistics() {
        return this._statistics;
    }
}
