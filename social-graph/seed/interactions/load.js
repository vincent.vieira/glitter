import { resolve as resolvePath } from 'path';
import { lines } from '../../readlines';

const interactions = {
    'RT': 'retweets',
    'MT': 'mentions',
    'RE': 'replies'
};

export async function loadInteractions(graph) {
    const filePath = resolvePath(__dirname, 'interactions.txt');

    console.log('Loading file...');
    for await(let line of lines(filePath)) {
        const elements = line.split(' ');
        const userId = Number(elements[0]);
        const otherUserId = Number(elements[1]);
        const interaction = interactions[elements[3]];

        await graph.query(`CREATE (:User {id: ${userId}}), (:User {id: ${otherUserId}})`);
        await graph.query(`MATCH (a:User), (b:User) WHERE (a.id = ${userId} AND b.id = ${otherUserId}) CREATE (a)-[:${interaction}]->(b)`);
    }
    console.log('File loaded...');

    // Sample data, if the file data set is too long to insert
    /*await graph.query('CREATE (:User {id: 223789})');
    await graph.query('CREATE (:User {id: 213163})');
    await graph.query('CREATE (:User {id: 376989})');
    await graph.query('CREATE (:User {id: 168366})');

    await graph.query('MATCH (a:User), (b:User) WHERE (a.id = 223789 AND b.id = 213163) CREATE (a)-[:retweets]->(b)');
    await graph.query('MATCH (a:User), (b:User) WHERE (a.id = 223789 AND b.id = 376989) CREATE (a)-[:retweets]->(b)');
    await graph.query('MATCH (a:User), (b:User) WHERE (a.id = 223789 AND b.id = 168366) CREATE (a)-[:retweets]->(b)');
    await graph.query('MATCH (a:User), (b:User) WHERE (a.id = 168366 AND b.id = 213163) CREATE (a)-[:retweets]->(b)');
    await graph.query('MATCH (a:User), (b:User) WHERE (a.id = 168366 AND b.id = 376989) CREATE (a)-[:retweets]->(b)');*/

    // Example query, it's now your time to shine ;)
    const result = await graph.query('MATCH (a {id: 223789})-[:retweets]->(b) return count(b)');
    const retweetsCount = result.next().get('count(b)');
}
