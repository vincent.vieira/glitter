import { createClient } from 'redis';
import { loadInteractions } from './seed/interactions/load';
import { RedisGraph } from 'redisgraph.js';
import './redisgraph-fix';

const redisClientConfig = {
    host: '127.0.0.1',
    port: 6379,
    // password: ''
};

const client = createClient(redisClientConfig);
const graph = new RedisGraph("interactions", client);

async function loadData() {
    console.log('Resetting schema...');
    await graph.deleteGraph();
    await loadInteractions(graph);
    // TODO : add others
}

loadData();

