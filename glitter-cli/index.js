/* eslint-disable no-console */
import chalk from 'chalk';
import clear from 'clear';
import figlet from 'figlet';
import inquirer from 'inquirer';
import commander from 'commander';

import { createNewUserProfile } from './prompts';

import { disconnectGlitter } from './glitter-api';

async function runCommandPrompt() {
	const availableCommands = [
		{
			filter(str) {
				return str.replace(/ \[.*$/, '');
			},
		},
		'home', 'quit',
	];

	const questions = [
		{
			type: 'command',
			name: 'cmd',
			autoCompletion: availableCommands,
			message: '>',
			context: 0,
			validate: (val) => (val
				? true
				: 'Press TAB for suggestions'),
			short: true,
		},
	];

	const answers = await inquirer.prompt(questions);

	if (answers.cmd === 'quit') {
		console.log('GoodBye 👋 !');

		disconnectGlitter();
	} else {
		console.log('Press TAB for suggestions');
		runCommandPrompt();
	}
}

// Fix default option, enforce -u if not using
// the create command.
commander
	.version('1.0.0')
	.command('create')
	.action(() => {
		createNewUserProfile().then(async () => {
			await disconnectGlitter();

			process.exit();
		});
	})
	.option('-u, --username <username>', 'Login as given username')
	.action(async (options) => {
		console.log(options.environment);

		await runCommandPrompt();
	});


clear();

console.log(chalk.yellowBright(figlet.textSync('Glitter', { horizontalLayout: 'full' })));

commander.parse(process.argv);
