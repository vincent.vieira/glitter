/* eslint-disable no-console */
import chalk from 'chalk';
import clear from 'clear';
import figlet from 'figlet';
import inquirer from 'inquirer';

import { createGlitterProfile, getGlitterProfile } from './glitter-api';

inquirer.registerPrompt(
	'command',
	require('inquirer-command-prompt'),
);

export const createNewUserProfile = async () => {
	const questions = [{
		type: 'input',
		name: 'username',
		message: 'What\'s your desired username?',
		validate: (value) => {
			// TODO: add a validation mecanism!
			if (value.length > 0) {
				return true;
			}

			return 'This username is not available!';
		},
	},
	{
		type: 'input',
		name: 'profile.displayName',
		message: 'What\'s your desired display name?',
		validate: (value) => {
			// TODO: add a validation mecanism!
			if (value.length > 0) {
				return true;
			}

			return 'This display name is invalid!';
		},
	},
	{
		type: 'input',
		name: 'profile.location',
		message: 'What\'s your location?',
		validate: (value) => {
			// TODO: can be improved! ;)
			if (value.length > 0) {
				return true;
			}

			return 'This location is invalid!';
		},
	}];

	const answers = await inquirer.prompt(questions);

	const success = await createGlitterProfile(
		answers.username, 
		answers.profile
	);

	if (!success) {
		console.log('An error has occured while trying to create the user profile!');

		return false;
	}

	const profile = await getGlitterProfile(answers.username);

	if (profile === null) {
		console.log('An error has occured while trying to retrieve the user profile!');

		return false;
	}

	console.log(profile);
	console.log(`Hello ${profile.displayName}!`);
	console.log('Your profile has been sucessfully created!');

	return true;
};
