/* eslint-disable no-console */
import { createClient } from 'redis';
import { promisify } from 'util';

const redisClientConfig = {
	// host: '127.0.0.1',
	// port: 6379,
	// password: ''
};

const redisClient = createClient(redisClientConfig);

const redisGet = promisify(redisClient.get).bind(redisClient);
const redisSet = promisify(redisClient.set).bind(redisClient);
const redisDel = promisify(redisClient.del).bind(redisClient);

export const disconnectGlitter = async () => {
	await redisClient.quit();
};

export const createGlitterProfile = async (username, profile) => {
	try {
		const profileValue = JSON.stringify({ username, profile });
		await redisSet(`profile:${username}`, profileValue);

		return true;
	} catch (error) {
		console.error(error);

		return false;
	}
};

export const deleteGlitterProfile = async (username) => {
	try {
		await redisDel(`profile:${username}`);

		return true;
	} catch (error) {
		console.error(error);

		return false;
	}
};

export const getGlitterProfile = async (username) => {
	try {
		const profileValue = await redisGet(`profile:${username}`);

		return JSON.parse(profileValue);
	} catch (error) {
		console.error(error);

		return null;
	}
};
