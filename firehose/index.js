import PubNub from 'pubnub';
import fs from 'fs';

const pubnub = new PubNub({
	subscribe_key: 'sub-c-78806dd4-42a6-11e4-aed8-02ee2ddab7fe',
});

pubnub.addListener({
	message(message) {
		fs.appendFileSync('./firehose.json', `${JSON.stringify(message.message)}\n`);
	},
});

pubnub.subscribe({
	channels: ['pubnub-twitter'],
});
