/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
import CLI from 'clui';
import CLC from 'cli-color';
import os from 'os';
import PubNub from 'pubnub';
import { createClient } from 'redis';
import { promisify } from 'util';

const redisClientConfig = {
	// host: '127.0.0.1',
	// port: 6379,
	// password: ''
};

const redisClient = createClient(redisClientConfig);

const redisPFCOUNT = promisify(redisClient.pfcount).bind(redisClient);
const redisPFADD = promisify(redisClient.pfadd).bind(redisClient);

const startTime = os.uptime();
let lastDrawUpdateTime = startTime;

console.debug(startTime);

let tweetCount = 0;
let tweetCountPerSec = 0;

const pubnub = new PubNub({
	subscribe_key: 'sub-c-78806dd4-42a6-11e4-aed8-02ee2ddab7fe',
});

pubnub.addListener({
	message(message) {
		++tweetCount;
		++tweetCountPerSec;

		redisClient.pfadd(startTime, message.message.user.id);
	},
});

pubnub.subscribe({
	channels: ['pubnub-twitter'],
});

const clear = CLI.Clear;
const { Line, Progress, Gauge, Sparkline } = CLI;

let uniqueUserCount = 0;
let drawTimeout;
let tweetSeries = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];

async function render() {
	clear();

	const blankLine = new Line().fill().output();

	const tweetCountLine = new Line()
		.padding(2)
		.column('Tweet Count', 20, [CLC.cyan])
		.column(tweetCount.toString(), 40)
		.fill()
		.output();

	blankLine.output();

	await redisClient.pfcount(startTime, function(err, count) {
        uniqueUserCount = count;

		const uniqueUserCountLine = new Line()
			.padding(2)
			.column('Unique User Count', 20, [CLC.cyan])
			.column(uniqueUserCount.toString(), 40)
			.fill()
            .output();
            
	    blankLine.output();
	});

	const elapsedTimeLine = new Line()
		.padding(2)
		.column('Elapsed time', 20, [CLC.cyan])
		.column(`${(os.uptime() - startTime).toString()} seconds`, 40)
		.fill()
		.output();

	tweetSeries.push(tweetCountPerSec);
	tweetCountPerSec = 0;
	tweetSeries.shift();
	const tweetSeriesLine = new Line()
		.padding(2)
		.column('Tweet/Sec', 20, [CLC.cyan])
		.column(Sparkline(tweetSeries, ' tweet/sec'), 80)
		.fill()
		.output();

	blankLine.output();

	drawTimeout = setTimeout(render, 1000);
}

render();

process.stdout.on('resize', async () => {
	clearTimeout(drawTimeout);

	await render();
});
